/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package higuys;

import java.awt.Dimension;
import java.awt.Toolkit;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.KeyListener;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;

/**
 *
 * @author zacnoo
 */
public class HiGuys implements State, KeyListener {

	Paddle paddle1, paddle2;
	Ball ball;

	/**
	 * @throws java.lang.InterruptedException
	 */
	public HiGuys() throws InterruptedException {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		ball = new Ball(screenSize.width / 2, screenSize.height / 2, 100, 100);
		paddle1 = new Paddle(100, 100, 50, 300);
		paddle2 = new Paddle(screenSize.width - 150, 100, 50, 300);
	}

	@Override
	public void keyReleased(int key, char c) {
		if (key == Input.KEY_W) {
			paddle1.setMovingUp(false);
		}
		if (key == Input.KEY_S) {
			paddle1.setMovingDown(false);
		}
		if (key == Input.KEY_UP) {
			paddle2.setMovingUp(false);
		}
		if (key == Input.KEY_DOWN) {
			paddle2.setMovingDown(false);
		}
	}

	@Override
	public void keyPressed(int key, char c) {
		if (key == Input.KEY_W) {
			paddle1.setMovingUp(true);
		}
		if (key == Input.KEY_S) {
			paddle1.setMovingDown(true);
		}
		if (key == Input.KEY_UP) {
			paddle2.setMovingUp(true);
		}
		if (key == Input.KEY_DOWN) {
			paddle2.setMovingDown(true);
		}
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		paddle1.update(context);
		paddle2.update(context);
		ball.update(context, paddle1, paddle2);
	}

	@Override
	public void render(Context cntxt, Graphics grphcs) throws SlickException {
		paddle1.render(cntxt, grphcs);
		paddle2.render(cntxt, grphcs);
		ball.render(cntxt, grphcs);
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO code application logic here
		HiGuys hg = new HiGuys();

		SlickEngine engine = new SlickEngine("Pong");
		engine.addState(hg);
		engine.getApp().setVSync(true);
		engine.startFullscreen();
	}

}
