/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package higuys;

import java.awt.Rectangle;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author zacnoo
 */
public class Ball {

	private int vSpeed, hSpeed;

	private final Rectangle bounds;
	private Color colour;

	public Ball(int x, int y, int width, int height) {
		bounds = new Rectangle(x, y, width, height);

		hSpeed = 5;
		vSpeed = 10;

	}

	public void update(Context context, Paddle paddle1, Paddle paddle2) {
		if (bounds.y + bounds.height >= context.getHeight()) {
			vSpeed = -vSpeed;
		}
		if (bounds.y < 0) {
			vSpeed = -vSpeed;
		}

		if (bounds.x > context.getWidth()
				|| bounds.x + bounds.width < 0) {
			reset(context);
		}

		if (paddle1.intersects(bounds)) {
			hSpeed = Math.abs(hSpeed);
			paddle1.bounce();
			colour = paddle1.getColour();
		}
		if (paddle2.intersects(bounds)) {
			hSpeed = -Math.abs(hSpeed);
			paddle2.bounce();
			colour = paddle2.getColour();
		}

		bounds.x += hSpeed;
		bounds.y += vSpeed;
	}

	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(colour);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	private void reset(Context context) {
		bounds.x = context.getWidth() / 2;
		bounds.y = context.getHeight() / 2;
		colour = Color.white;
	}
}
