/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package higuys;

import java.awt.Rectangle;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author zacnoo
 */
public class Paddle {

	private boolean movingUp;
	private boolean movingDown;
	private Rectangle bounds;
	private Color colour = Color.cyan;

	public Paddle(int x, int y, int width, int height) {
		bounds = new Rectangle(x, y, width, height);
	}

	public void update(Context context) {
		if (isMovingUp()) {
			bounds.y -= 5;
		}
		if (isMovingDown()) {
			bounds.y += 5;
		}

		if (bounds.y < 0) {
			bounds.y = 0;
		}
		if (bounds.y > context.getHeight() - bounds.height) {
			bounds.y = context.getHeight() - bounds.height;
		}
	}

	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(colour);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	public Rectangle getLocation() {
		return bounds;
	}

	public boolean isMovingUp() {
		return movingUp;
	}

	public void setMovingUp(boolean movingUp) {
		this.movingUp = movingUp;
	}

	public boolean isMovingDown() {
		return movingDown;
	}

	public void setMovingDown(boolean movingDown) {
		this.movingDown = movingDown;
	}

	public boolean intersects(Rectangle ball) {
		return bounds.intersects(ball);
	}

	public void bounce() {
		int colCode = java.awt.Color.HSBtoRGB((float) Math.random(), 1, 1);
		java.awt.Color awtColour = new java.awt.Color(colCode);

		colour = new Color(awtColour.getRed(),
				awtColour.getGreen(),
				awtColour.getBlue());
	}

	public Color getColour() {
		return colour;
	}
}
